FROM maven:3.6-jdk-8 AS build-stage
COPY src /app/src
COPY pom.xml /app
WORKDIR /app
RUN mvn --batch-mode -DskipTests package

FROM openjdk:8-jre-alpine
COPY --from=build-stage /app/target/*.jar /app/app.jar
WORKDIR /app
ENTRYPOINT java -jar app.jar
EXPOSE 8080

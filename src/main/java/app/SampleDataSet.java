package app;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.stereotype.Component;

@Component
public class SampleDataSet implements CommandLineRunner {

    private static final String INDEX_NAME = "telepaton";
    private static final String INDEX_TYPE = "input";

    @Autowired
    InputRepository repository;

    @Autowired
    ElasticsearchTemplate template;

    @Override
    public void run(String... args) throws Exception {
        if (!template.indexExists(INDEX_NAME)) {
            template.createIndex(INDEX_NAME);
        }
        ObjectMapper mapper = new ObjectMapper();
        List<IndexQuery> queries = new ArrayList<>();
        List<Input> inputs = inputs();
        for (Input input : inputs) {
            IndexQuery indexQuery = new IndexQuery();
            indexQuery.setId(input.getId().toString());
            indexQuery.setSource(mapper.writeValueAsString(input));
            indexQuery.setIndexName(INDEX_NAME);
            indexQuery.setType(INDEX_TYPE);
            queries.add(indexQuery);
        }
        if (queries.size() > 0) {
            template.bulkIndex(queries);
        }
        template.refresh(INDEX_NAME);
    }

    private List<Input> inputs() {
        List<Input> inputs = new ArrayList<>();
        inputs.add(Input.builder().id(IdGenerator.nextId()).bairro("Barra").build());
        inputs.add(Input.builder().id(IdGenerator.nextId()).bairro("Tororó").build());
        inputs.add(Input.builder().id(IdGenerator.nextId()).bairro("Lapa").build());
        return inputs;
    }
}

package app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @Autowired
    private InputRepository repository;

    @PostMapping
    public ResponseEntity<String> post(@RequestBody Input input) {
        input.setId(IdGenerator.nextId());
        repository.save(input);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}

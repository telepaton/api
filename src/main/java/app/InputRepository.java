package app;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InputRepository extends ElasticsearchRepository<Input, Long> {

}

package app;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
@Document(indexName = "telepaton", type = "input")
public class Input {

    @Id
    private Long id;

    private String cpf;

    private String rg;

    private String nome;

    private String celular;

    private String nascimento;

    private String email;

    private String cidade;

    private String bairro;

    private String endereco;

    private String genero;

    private String renda;

    private String corOuRaca;
}

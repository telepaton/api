package app;

public class IdGenerator {

    private static Long latest = 0L;

    public static Long nextId() {
        return latest++;
    }
}
